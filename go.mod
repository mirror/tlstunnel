module git.sr.ht/~emersion/tlstunnel

go 1.18

require (
	git.sr.ht/~emersion/go-scfg v0.0.0-20240128091534-2ae16e782082
	github.com/caddyserver/certmagic v0.20.0
	github.com/libdns/dnsupdate v0.0.0-20230728193621-2e79c50ea2ee
	github.com/libdns/libdns v0.2.1
	github.com/pires/go-proxyproto v0.7.0
	go.uber.org/zap v1.27.0
)

require (
	github.com/klauspost/cpuid/v2 v2.2.7 // indirect
	github.com/mholt/acmez v1.2.0 // indirect
	github.com/miekg/dns v1.1.58 // indirect
	github.com/zeebo/blake3 v0.2.3 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/mod v0.16.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/tools v0.19.0 // indirect
)
